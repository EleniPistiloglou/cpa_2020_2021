package tme1_cpa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

public class Tme1_q1 {

	public static Set<Integer> nodes(File file) 
	//returns the set of nodes of the graph read from file
	{
		Set<Integer> set = new HashSet<Integer>();
		
		try
		  {
		    BufferedReader reader = new BufferedReader(new FileReader(file));
		    String line;
		    while ((line = reader.readLine()) != null){
		    	if(line.charAt(0) != '#') {
			    	int pos = line.indexOf("\t");
			    	String first = line.substring(0,pos);
			    	String second = line.substring(pos+1,line.length());
			    	int node1 = Integer.valueOf(first);
			    	int node2 = Integer.valueOf(second);
			    	if (!set.contains(node1)) set.add(node1); 
			    	if (!set.contains(node2)) set.add(node2); 
		    	}
		    }
		    reader.close();
		  }
		  catch (Exception e)
		  {
		    System.err.format("Exception occurred trying to read '%s'.", file);
		    e.printStackTrace();
		  }

		return set;
	}
	
	public static int[] nbr_of_nodes_and_edges(File file) 
	// returns a 2*1 table containing the number of nodes and edges of the graph read from file 
	{

		Set<String> set = new HashSet<String>();
		int nbr_edges = 0;
		
		try
		  {
		    BufferedReader reader = new BufferedReader(new FileReader(file));
		    String line;
		    while ((line = reader.readLine()) != null){
		    	if(line.charAt(0) != '#') {
		    		nbr_edges++;
			    	int pos = line.indexOf("\t");
			    	String first = line.substring(0,pos);
			    	String second = line.substring(pos+1,line.length());
			    	if (!set.contains(first)) set.add(first); 
			    	if (!set.contains(second)) set.add(second); 
		    	}
		    }
		    reader.close();
		  }
		  catch (Exception e)
		  {
		    System.err.format("Exception occurred trying to read '%s'.", file);
		    e.printStackTrace();
		  }
		
		int[] res = new int[2];
		res[0] = set.size();  // nbr of nodes
		res[1] = nbr_edges;  
		/*
		List<String> list = new ArrayList<String>(set);
		Collections.sort(list);
		for(int i = 0 ; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		*/
		return res;
	}
	
	
	public static void main(String[] args) {
		
		File file;  int[] nbr_n_e;
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt");
		nbr_n_e = nbr_of_nodes_and_edges(file);
		System.out.println("the amazon graph contains " + nbr_n_e[0] + " nodes and " + 
				nbr_n_e[1] + " edges.\n");    // 334.863 and 925.872 (~ 1 sec)
		/*
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-lj.ungraph.txt");
		nbr_n_e = nbr_of_nodes_and_edges(file);
		System.out.println("the live journal graph contains " + nbr_n_e[0] + " nodes and " + 
				nbr_n_e[1] + " edges.\n");  // 3.997.962 and 34.681.189  (~ 5 sec)
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-orkut.ungraph.txt");
		nbr_n_e = nbr_of_nodes_and_edges(file);
		System.out.println("the orkut graph contains " + nbr_n_e[0] + " nodes and " + 
				nbr_n_e[1] + " edges.\n");  // 3.072.441 and 117.185.083  (~ 25 sec)
		*/

	}

}
