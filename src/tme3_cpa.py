"""
tme3
"""
import copy
import time

import numpy as np
import random
import matplotlib.pyplot as plt

################################# elapsed time #############################################

# for 80000 nodes :
# 4.740370750427246 - 12.49745512008667  seconds

# for 160000 nodes :
# 21.708252429962158 seconds

############################################################################################


def ex1(proba=50, proba2=5, n=400, plot=False):
    """
    Creates benchmarks of 4 dense communities of n/4 nodes each
    with parameters p = proba/1000 and q = proba2/1000.
    Gives random positions to the nodes and prints the graph using matplotlib.pyplot .
    """

    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\11_3\\"

    # nodes = np.array([x for x in range(400)]) # labels of nodes
    adj = [ [] for x in range(n) ] # initialization of adjacency array
    nbr_nodes = n//4  # number of nodes in the same community

    ### connecting nodes in the same partition ###
    for partition in range(4):  # for each of the partitions
        for node in range(partition*nbr_nodes,partition*nbr_nodes+nbr_nodes-1):   # for each node of the partition
            for node2 in range(partition * nbr_nodes, partition * nbr_nodes + nbr_nodes-1):  # for each node of the partition
                if(random.randint(0,1000) <= proba): # connect nodes
                    adj[node].append(node2)
                    adj[node2].append(node) # the graph isn't directed
    
    ### connecting nodes between partitions ###
    for partition in range(4):  # for each of the partitions
        for node in range(partition*nbr_nodes,partition*nbr_nodes+nbr_nodes-1):   # for each node of the partition
            candidates = [x for x in range(partition * nbr_nodes)]
            candidates.extend(range(partition * nbr_nodes + nbr_nodes, n))
            for node2 in candidates:  # for each node in other partitions
                if(random.randint(0,1000) <= proba2):
                    adj[node].append(node2)
                    adj[node2].append(node)
    
    ### saving benchmark ###
    adj_np = np.array(adj, dtype=object)
    np.save(path + "ex1_n_" + str(n) + "_pq_"+str(proba/proba2)+"_adj.npy", adj_np)
    
    ### printing clusters of nodes ###
    pos = []
    for x in range(nbr_nodes):
        x = random.randint(0,100)
        y = random.randint(0,100)
        pos.append([x,y])
        if plot : plt.plot(x,y,'bo')
    for x in range(nbr_nodes, nbr_nodes*2):
        x = random.randint(150,250)
        y = random.randint(0, 100)
        pos.append([x, y])
        if plot : plt.plot(x, y, 'bo')
    for x in range(nbr_nodes*2, nbr_nodes*3):
        x = random.randint(0,100)
        y = random.randint(150, 250)
        pos.append([x, y])
        if plot : plt.plot(x, y, 'bo')
    for x in range(nbr_nodes*3, n):
        x = random.randint(150,250)
        y = random.randint(150, 250)
        pos.append([x, y])
        if plot : plt.plot(x, y, 'bo')

    ### saving positions ###
    np.save(path + "ex1_n_" + str(n) + "_pq_"+str(proba/proba2)+"_pos.npy", adj_np)
    
    ### printing edges ###
    if plot :
        for node in adj:
            for neighbour in node:
                plt.plot( [ pos[adj.index(node)][0], pos[neighbour][0] ], [ pos[adj.index(node)][1], pos[neighbour][1] ] , color='black', linewidth = 0.5)

        plt.show()



def ex2(n, file_prefix = "benchmark_"):
    """
    implements label propagation algorithm and saves the labels in a file
    :param n: number of nodes in the graph
    :return: nothing
    """

    ### loading data ###

    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\"

    adj = np.load(path + file_prefix + str(n) + "_adj.npy", allow_pickle=True).tolist()
    print("adj loaded")

    if file_prefix == "lfr\\lfr_" :
        n = n+1

    ### 1. giving a unique label to each node in the network ###
    labels = [x for x in range(n)]

    ### 3. setting a node's label to a label occurring with the highest frequency among its neighbours ###
    starting_time = time.time()
    counter = 0 # set to true if at least one node in each iteration changes its label, set ot false otherwise
    while counter < len(adj):
        counter = 0
        nodes = [x for x in range(n)] # n+1 for lfr, n for the other benchmarks
        random.shuffle(nodes)  # step 2
        for node in nodes:
            label = [] # contains the neighbours' labels
            score = [] # score[i] contains the number of times the label in label[i] occurs
            for neighbour in adj[node]:
                if labels[neighbour] in label:
                    score[label.index(labels[neighbour])] += 1
                else:
                    label.append(labels[neighbour])
                    score.append(1)
            if len(adj[node]) > 0:
                max_score = max(score)
                new_label = label[score.index(max_score)]
                if labels[node] != new_label:
                    labels[node] = new_label
                else:
                    counter += 1
            else: counter += 1

    print("process finished after ", time.time()-starting_time, " seconds")  #  21.708252429962158 sec for 160000

    ### saving results ###

    if file_prefix == "lfr\\lfr_" : n = n-1

    np.save(path + file_prefix + "lp_" + str(n) + "_labels__new_new.npy", np.array(labels))
    with open(path + file_prefix + "lp_" + str(n) + "_labels_new_new.txt", 'w') as file:
        for l in labels :
            file.write(str(l)+'\n')
    
    print("result saved")


def affichage(n, file_prefix = "benchmark_", algo="lp_"):
    """
    plots the graph using pyplot
    """

    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\11_3\\"

    # loading data  and assigning colors
    labels = np.load(path + file_prefix + algo + str(n) + "_labels.npy", allow_pickle=True)
    print("labels loaded")

    colors=["blue", "red", "green", "yellow", "cyan", "pink", "purple", "darkorange", "lime", "turquoise", "magenta", "sienna", "grey", "silver", "whitesmoke", "wheat", "lightcyan", "seagreen", "tan", "moccasin", "goldenrod", "darkkhaki", "olive", "palegreen", "azure", "darkslategrey", "powderblue", "darkslateblue", "thistle", "violet", "crimson", "orchid"]
    label_names = [] # helps in coloring
    for l in labels:
        if l not in label_names:
                label_names.append(l)
    print("nbr of label names : ", len(label_names))
    print("nbr of colors : ", len(colors))

    adj = np.load(path + file_prefix + str(n) + "_adj.npy", allow_pickle=True)
    print("adj loaded")

    pos = np.load(path + file_prefix + str(n) + "_pos.npy", allow_pickle=True).tolist()
    print("positions loaded")

    # printing edges
    for i in range(n):
        node = adj[i]
        for neighbour in node:
            plt.plot( [ pos[i][0], pos[neighbour][0] ], [ pos[i][1], pos[neighbour][1] ] , color='black', linewidth = 0.5)

    # printing nodes
    for node in range(n):
        c = colors[label_names.index(labels[node]) % len(colors)]
        plt.plot(pos[node][0], pos[node][1], 'o', color=c)

    plt.show()
    




#ex1(n=4000)
ex2(80000, file_prefix="benchmark_")
#affichage(8000, file_prefix = "benchmark_", algo="lv_")

