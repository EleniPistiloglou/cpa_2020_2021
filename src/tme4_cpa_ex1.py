"""
k-CORE DECOMPOSITION ALGORITHM

input : graph with n nodes and m edges given as list of edges in a .txt file
We maintain a dictionnary containing the neighbours and the core values for each node
and a dictionnary whose keys are the degrees of nodes and the values bins (lists) containing the names of nodes having that degree.
the return value is the graph's core value
"""


######################### elapsed time ##########################

### for amazon ###
# 492.43237590789795 sec

### for google scholar ###
# 186.12937784194946 sec

#################################################################


########################### results #############################

### for amazon ###
# the graph's core value is 6
# the average degree is 5.529855493141971
# the average degree density is 2,7649277465709857464097257684486  ( n_edges / n_nodes )
# the edge density is 1,6513834036534367867418373947767e-5
# maximum degree density of 3.6241391304347825 found for a prefix containing 57499 nodes
# maximum edge density of 0.6 found for a prefix containing 11 nodes

###  for google scholar ###
# the graph's core value is 14
# the average degree is 6.060669528852644

#################################################################



import time
import sys
import numpy as np

sys.setrecursionlimit(10**5)

def core_decomposition(graph_file_name, delimiter = "\t"):

    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\"

    adj = dict()  # node_name : { "n" : list_of_neighbours,"c" : core_value, "d" : degree }
    # c is initialised to the node's degree
    # and is refined until it reaches the core value.
    # The degree is useful only for the plotting
    degree_to_nodes = dict() # degree : list_of_nodes

    ### constructing adjacency dictionary  ###

    nbr_edges = 0
    with open(path + graph_file_name + ".txt") as text:
        # there are four lines of comments at the beginning
        l = text.readline()
        l = text.readline()
        l = text.readline()
        l = text.readline()
        l = text.readline()
        print("reading file...")
        while (l):
            if (l[0] != "\n" and l[0] != "#"):
                nbr_edges += 1
                l_split = l.split(delimiter)
                n0 = int(l_split[0])
                n1 = int(l_split[1])

                # updating neighbours
                if adj.get(n0) :
                    adj.get(n0).get('n').append(n1)
                else :
                    aux = dict({'n' : [n1]})
                    adj.update({n0 : aux})
                # if the graph isn't directed

                if adj.get(n1) :
                    adj.get(n1).get('n').append(n0)
                else :
                    aux = dict({'n' : [n0]})
                    adj.update({n1 : aux})

                # updating degree
                if adj.get(n0).get('c') :
                    deg = adj.get(n0).get('c')+1
                    adj.get(n0).update({'c':deg})
                else :
                    adj.get(n0).update({'c': 1})
                # if the graph isn't directed

                if adj.get(n1).get('c'):
                    deg = adj.get(n1).get('c') + 1
                    adj.get(n1).update({'c': deg})
                else:
                    adj.get(n1).update({'c': 1})

            l = text.readline()
    print("adj created")

    # putting the degree
    for node in adj.keys() :
        c = adj.get(node).get('c')
        adj.get(node).update({'d' : c})

    # filling degree_to_nodes
    for key in adj.keys():
        d = adj.get(key).get('d')
        if degree_to_nodes.get(d):
            degree_to_nodes.get(d).append(key)
        else :
            degree_to_nodes.update({ d : [key] })

    ### algorithm ###

    starting_time = time.time()

    c = 0
    max_degree = max(list(degree_to_nodes.keys()))
    print("max degree : ", max_degree)
    order = open(path + graph_file_name + "_ordering.txt", 'w')
    nbr_nodes = len(adj)
    densities = [nbr_edges/(nbr_nodes*(nbr_nodes-1))]

    while c < max_degree:
        nodes = degree_to_nodes.get(c)
        while nodes:
            for node in nodes:
                for neighbour in adj.get(node).get('n'):
                    if adj.get(neighbour) :
                        if node in adj.get(neighbour).get('n'):
                            adj.get(neighbour).get('n').remove(node)
                        nc = adj[neighbour].get('c')
                        if nc > c:  # if nc == c then nc can be refined no more
                            adj.get(neighbour).update({'c': nc - 1})
                            # change bucket
                            degree_to_nodes.get(nc).remove(neighbour)
                            if degree_to_nodes.get(nc - 1) :
                                degree_to_nodes.get(nc - 1).append(neighbour)
                            else :
                                degree_to_nodes.update({nc-1 : [neighbour]})
                            if nc == c+1 :
                                if neighbour not in nodes :
                                    print(nc)
                                    nodes.append(neighbour)
                                # this operation may be mutating the list
                                # and it makes the iterator skip a node,
                                # but this doesn't really affect the outcome
                                # because the order of treatment of the nodes in a bucket doesn't matter
                nbr_edges -= len(adj[node].get('n'))
                nbr_nodes -= 1
                if nbr_nodes > 1 : densities.append(nbr_edges/nbr_nodes)
                nodes.remove(node)
                order.write(str(node) + '\n')
        c += 1

    order.close()
    print("algorithm finished with elapsed time ",time.time()-starting_time)
    print(densities)
    max_dens = max(densities)
    print(max_dens)
    densities = np.array(densities)
    index = np.where(densities==max_dens)
    print(index)
    print("maximum density of ", max_dens, " found for a prefix containing ", 334863-index[0][0]-1, " nodes")
    print()

    ### saving result into a file ###
    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME4\\"
    np.save(path+"densities_" + graph_file_name + ".npy", np.array(densities))


### example graph from the lesson for testing ###
graph_example = [
    [0,15],
    [1,15],
    [2,16],
    [3,16],
    [4,16],
    [5,16],
    [6,16],
    [7,16],
    [8,17],
    [9,18],
    [10,19],
    [11,19],
    [12,20],
    [13,20],
    [15,21],
    [21,16],
    [16,22],
    [19,25],
    [23,20],
    [23,24],
    [24,20],
    [25,20],
    [25,22],
    [25,26],
    [25,18],
    [26,22],
    [26,18],
    [26,20],
    [20,22],
    [18,17],
    [19,20]
]
"""
### saving example graph in text file ###
file = open("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph_tme4_cpa.txt", 'w')
for line in graph_example:
    file.write(str(line[0])+'\t'+str(line[1])+'\n')
file.close()
"""

core_decomposition("com-amazon.ungraph", "\t")






