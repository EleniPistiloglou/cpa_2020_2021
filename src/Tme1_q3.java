package tme1_cpa;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;
import java.util.Set;

public class Tme1_q3 {
	
	
	public static int[] bfs(HashMap<Integer,ArrayList<Integer>> adj, int node, int n) {
		
		int last = node; // last node visited
		int[] mark = new int[n]; // -1 if node not yet visited, path length between node and i otherwise
		for(int i = 0 ; i<n ; i++) mark[i] = -1;
		Queue<Integer> queue = new ArrayDeque<>();  
		queue.add(node);
		mark[node] = 0; // node is the root
				
		while(!queue.isEmpty()) {
			last = queue.remove();
			for(int neighbour : adj.get(last)) {
				if(mark[neighbour] == -1) {
					queue.add(neighbour);
					mark[neighbour] = mark[last]+1;
				}
			}
		}
		
		// returning the node furthest from the node passed as argument and the distance between them 
		int[] res = new int[2];
		res[0] = last;
		res[1] = mark[last];
		return res;
		
	}
	
	public static int diameter_lb(File file) {

		Set<Integer> nodes = Tme1_q1.nodes(file);
		HashMap<Integer,ArrayList<Integer>> adj = Tme1_q2.file_to_adjacency_array(file);

		// finding the maximum node name (n) needed as argument to bfs function
		int n = 0;
		for(int node : nodes)
			if(node>n) n=node;

		// finding the furthest node from n (node2). n+1 will be the size of the array storing the distances)
		int node2 = bfs(adj, n, n+1)[0];
		
		// returning the diameter's lower bound by finding the furthest node from node2
		return bfs(adj, node2, n+1)[1];
		
	}
	
	public static int diameter_ub(File file) {
		Set<Integer> nodes = Tme1_q1.nodes(file);
		HashMap<Integer,ArrayList<Integer>> adj = Tme1_q2.file_to_adjacency_array(file);

		// finding the maximum node name needed to determine the size of the matrix visited
		int n = 0;
		for(int node1 : nodes)
			if(node1>n) n=node1;
		boolean[] visited = new boolean[n+1]; // false if i not yet visited, true otherwise
		
		//initialization
		for(int i = 0 ; i<n+1 ; i++)
			visited[i] = false;
		
		// constructing a spanning tree and storing it as an adjacency array (not optimized to prefer the nodes with larger number of neighbours)
		HashMap<Integer,ArrayList<Integer>> tree = new HashMap<>(); 
		
		for(int key : adj.keySet())
			tree.put(key, new ArrayList<>());
		
		Queue<Integer> queue = new ArrayDeque<>();   
		queue.add(n);  // n is the tree's root
		visited[n] = true;
		while(!queue.isEmpty()) {
			int last = queue.remove();
			ArrayList<Integer> newNeighbours = tree.get(last);
			for(int neighbour : adj.get(last)) {
				if(!visited[neighbour]) {
					queue.add(neighbour);
					visited[neighbour] = true;
					newNeighbours.add(neighbour); 
					ArrayList<Integer> newNeighbourNeighbours = tree.get(neighbour);
					newNeighbourNeighbours.add(last);
					tree.put(neighbour, newNeighbourNeighbours);
				}
				if(newNeighbours.size() != 0) tree.put(last, newNeighbours);
			}
		}

		// finding an upper bound for the diameter using the tree (same procedure as for lower bound)
		int node2 = bfs(tree, n, n+1)[0];
		return bfs(tree, node2, n+1)[1];
		
	}
	
	public static void main(String[] args) {
		File file;
		/*
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph2.txt");	
		System.out.println(diameter_lb(file));  System.out.println();  // ok //
		System.out.println(diameter_ub(file));  System.out.println();  // ok //
		 
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph1.txt");	
		System.out.println(diameter_lb(file));  System.out.println();  // ok //
		System.out.println(diameter_ub(file));  System.out.println();  // ok //
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph4.txt");	
		System.out.println(diameter_lb(file));  System.out.println();  // ok //
		System.out.println(diameter_ub(file));  System.out.println();  // ok //
		*/
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt");
		long startTime = System.currentTimeMillis();
		System.out.println(diameter_lb(file));
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println(elapsedTime+" milliseconds\n");  // 47  (1,643  seconds) //
		//System.out.println(diameter_ub(file));  System.out.println();  // 59  (2,084 seconds) //
		/*
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\k5.txt");	
		System.out.println(diameter_lb(file));  System.out.println();  // ok //
		System.out.println(diameter_ub(file));  System.out.println();  // ok //
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-lj.ungraph.txt");	
		System.out.println(diameter_lb(file));  System.out.println();  //  OutOfMemoryError //
		*/
	}
}
