"""
contains the functions udeful for comparing the community structure of two graphs
"""


import numpy as np
import matplotlib.pyplot as plt


def detect(n=80000, algo = 'lp_', test="lfr_"):
    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\"
    file = test +algo + str(n) + "_labels__new_new.npy"


    l = np.load(path+file)

    labels = dict()

    for node in range(len(l)):
        if l[node] in labels.keys():
            labels.get(l[node]).append(node)
        else:
            labels.update({ l[node] : [node] })

    print(len(labels.keys()), " communities detected.")

    lengths = np.zeros(n+1)
    for key in labels.keys():
        lengths[len(labels.get(key))] += 1

    lengths_ = np.where(lengths!=0)[0]
    frequency = lengths[lengths_]

    print(algo, ' ', test, ' ', str(n))
    print()

    print("frequencies : \n", frequency)
    print()
    print("sizes : \n", lengths_)

    plt.plot(lengths_, frequency, 'bo')
    plt.show()


def lfrfile_to_adjnpy(n):
    path = "C:\\Users\\elenp\\Downloads\\LFR-Benchmark_UndirWeightOvp-master\\" + str(n) + "\\"
    file = "network" + str(n) + ".txt"
    adj = [ [] for i in range(n+1) ]
    with open(path+file) as file :
        line = file.readline()
        while line:
            if line[0]!='#' and len(line)>0:
                s = line.split('\t')
                adj[int(s[0])].append(int(s[1]))
            line = file.readline()

    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\lfr\\lfr_" + str(n) + "new_new_adj.npy", np.array(adj))


def lv_labels_txt_to_npy():
    labels = []
    with open("D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\lfr\\lfr_lv_new_80000_labels.txt", 'r') as file :
        line = file.readline()
        while line:
            labels.append(int(line.split(' ')[1]))
            line = file.readline()
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\lfr\\lfr_lv_80000_labels.npy", np.array(labels))


def lp_labels_txt_to_npy():
    labels = []
    with open("D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\benchmark_lp_80000_labels.txt", 'r') as file :
        line = file.readline()
        while line:
            labels.append(int(line))
            line = file.readline()
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\benchmark_lp_80000_labels.npy", np.array(labels))



def lfrfile_to_edges_file(n):
    path = "C:\\Users\\elenp\\Downloads\\LFR-Benchmark_UndirWeightOvp-master\\"+str(n)+"\\"
    file = "network"+str(n)+".txt"
    edges = open("C:\\Users\\elenp\\Downloads\\louvain\\gen-louvain\\adjacency_array_"+str(n)+"_lfr.txt", 'w')

    with open(path+file) as file :
        line = file.readline()
        while line:
            if line[0]!='#' and len(line)>0:
                s = line.split(' ')
                edges.write(s[0] + " " + s[1] + "\n")
                line = file.readline()

    edges.close()


def lfr_text_to_edges(n):
    path = "C:\\Users\\elenp\\Downloads\\LFR-Benchmark_UndirWeightOvp-master\\" + str(n) + "\\network" + str(n) + ".txt"
    file = open(path, 'r')
    with open("C:\\Users\\elenp\\Downloads\\louvain\\gen-louvain\\adjacency_array_" + str(n) + ".txt", 'w') as dest:
        line = file.readline()
        while line :
            if line[0]!="#" and len(line)>0:
                s = line.split('\t')
                dest.write(s[0] + " " + s[1] + "\n")
            line = file.readline()
    file.close()

lfr_text_to_edges(80000)




detect(80000, algo = "lp_", test = "benchmark_")

