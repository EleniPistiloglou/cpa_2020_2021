"""
algorithm for creating random graphs
with a known structure, a given probability p and q and given size for each community (the sizes are given in thousands of nodes).

"""

import time
import numpy as np
import random
import matplotlib.pyplot as plt

def benchmark(n, path, community_sizes, proba=400, proba2=2):

    adj = [ [] for x in range(n) ] # initialization of adjacency array

    ### connecting nodes in the same subset ###
    last = 0 # index of last node created +1
    for subset in range(n//10000):  # for each of the subsets
        for node in range(last, last+community_sizes[subset] * 1000):   # for each node of the subset
            for node2 in range(last, last+community_sizes[subset] * 1000):  # for each node of the subset
                if(random.randint(0,1000000) <= proba): # connect nodes
                    adj[node].append(node2)
                    adj[node2].append(node)
        last += community_sizes[subset] * 1000
        print("subset : ",subset, "  last : ",last)
    print("edges within the same community have been created.")

    ### connecting nodes between partitions ###
    last = 0 # index of last node created +1
    for subset in range(n//10000):  # for each of the partitions
        for node in range(last, last+community_sizes[subset] * 1000):   # for each node of the subset
            candidates = [x for x in range(last)] # nodes before the current subset
            candidates.extend(range(last+community_sizes[subset] * 1000, n)) # nodes after the current subset
            for node2 in candidates:  # for each node in other subsets
                if(random.randint(0,1000000) <= proba2):
                    adj[node].append(node2)
                    adj[node2].append(node)
        last += community_sizes[subset] * 1000
        print("subset : ",subset, "  last : ",last)
    print("edges between different communities have been created.")

    np.save(path+"benchmark_"+str(n)+"_pq_"+str(proba/proba2)+"_adj.npy", np.array(adj))
    print("file adj saved")

    ### giving positions to nodes ###
    pos = []
    last = 0
    y_index = -1
    for subset in range(n//10000):
        if subset%4 == 0 : y_index += 1
        for i in range(last, last+1000*community_sizes[subset]): # for each node i in the community
            x = random.randint((subset%4)*1000, (subset%4+1)*1000-100) # width of each community : 900
            y = random.randint(y_index*1000, (y_index+1)*1000-100) # height of each community : 900
            pos.append([x,y])
        last += 1000*community_sizes[subset] # index of last node seen + 1
        
    np.save(path+"benchmark_"+str(n)+"_pos.npy", np.array(pos))
    print("file pos saved")

    # saving th input for the program for louvain
    print("adj size : ", len(adj))
    f = open('C:\\Users\\elenp\\Downloads\\louvain\\gen-louvain\\adjacency_array_'+str(n)+'_9_3.txt','w')
    for line in adj:
        for node in line:
            f.write(str(adj.index(line))+" "+str(node)+"\n")
    f.close()
    print("file for c++ saved")




benchmark(80000, "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\9_3\\", [8, 13, 2, 20, 10, 6, 12, 9])