package tme1_cpa;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;  

public class Tme1_q2 {
	
	public static HashMap<Integer,ArrayList<Integer>> file_to_adjacency_array(File file) {
		
		List<int[]> edges = list_of_edges(file);
		HashMap<Integer,ArrayList<Integer>> neighbours = new HashMap<>();  // key : node  value : array of node's neighbours

		for(int[] edge : edges) {
			ArrayList<Integer> array1;
			ArrayList<Integer> array2;
			// adding neighbour to the first node of the edge
			if(neighbours.containsKey(edge[0])) {
				array1 = neighbours.get(edge[0]);
			}else {
				array1 = new ArrayList<Integer>();
			}
			array1.add(edge[1]);
			neighbours.put(edge[0], array1);
			// adding neighbour to second node of the edge
			if(neighbours.containsKey(edge[1])) {
				array2 = neighbours.get(edge[1]);
			}else {
				array2 = new ArrayList<Integer>();
			}
			array2.add(edge[0]);
			neighbours.put(edge[1], array2);
		}
		
		return neighbours;		
	}

	
	public static List<int[]> list_of_edges(File file){
		
		List<int[]> list = new ArrayList<int[]>();
		try
		  {
		    BufferedReader reader = new BufferedReader(new FileReader(file));
		    String line;
		    while ((line = reader.readLine()) != null){
		    	if(line.charAt(0) != '#') {
			    	int pos = line.indexOf("\t");
			    	String first = line.substring(0,pos);
			    	String second = line.substring(pos+1,line.length());
			    	int[] res = new int[2];
			    	res[0] = Integer.parseInt(first);
			    	res[1] = Integer.parseInt(second);
			    	list.add(res);
		    	}
		    }
		    reader.close();
		  }
		  catch (Exception e)
		  {
		    System.err.format("Exception occurred trying to read '%s'.", file);
		    e.printStackTrace();
		  }
		
		return list;
	}
	
	public static boolean[][] file_to_adjacency_matrix(File file) {
		
		/*   renaming nodes to avoid empty rows   */
		
		Set<Integer> nodes = Tme1_q1.nodes(file);
		Integer name = 0;
		HashMap<Integer, Integer> newName = new HashMap<Integer, Integer>();
		for(int node : nodes) {
			newName.put(node, name++);
		}
		
		// verifying result
		for(int node : nodes) {
			System.out.println(node + " : " + newName.get(Integer.valueOf(node)));
		}
		
		/*   creating adjacency matrix   */
		
		List<int[]> edges = new ArrayList<int[]>(); // edges of graph
		edges = list_of_edges(file);
		int n = nodes.size();
		boolean[][] adj = new boolean[n][n];  //  adjacency matrix
				
		// edges
		System.out.println("EDGES");
		for(int i = 0 ; i<edges.size() ; i++) {
			System.out.print(newName.get(edges.get(i)[0]) + ", " + newName.get(edges.get(i)[1]) + "\n");
		}
		System.out.println();
		
		// initialization
		for(int i = 0 ; i<n ; i++) {
			for(int j = 0 ; j<n ; j++) {
				adj[i][j] = false;
			}
		}
		
		// filling matrix with true if i,j adjacent 
		for(int[] edge : edges) {
			adj[newName.get(edge[0])][newName.get(edge[1])] = true;
			adj[newName.get(edge[1])][newName.get(edge[0])] = true;
		}
		
		return adj;
	}
	

	
	public static void main(String[] args) {
		
		File file;
		HashMap<Integer,ArrayList<Integer>> adjArray;
		boolean[][] adjMatrix;
		int n ;
		List<int[]> l;
		
		/////////////////// testing edge list ////////////////
		/*
		long startTime = System.currentTimeMillis();
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt");  
		l = Tme1_q2.list_of_edges(file);
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("elapsed time : " + elapsedTime + "\n");    // 349 ms
		// verifying result
		for(int[] i : l)
			System.out.println(i[0]+", "+i[1]);
		
		long startTime = System.currentTimeMillis();
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-lj.ungraph.txt");  
		l = Tme1_q2.list_of_edges(file);
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("elapsed time : " + elapsedTime + "\n");    // 10.687 ms // heap 2G
		
		long startTime = System.currentTimeMillis();
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-orkut.ungraph.txt"); 
		l = Tme1_q2.list_of_edges(file);
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("elapsed time : " + elapsedTime + "\n");    // 61.907 ms  // heap 4G
		
		/////////// testing adjacency array ///////////
		
		// testing on a simple graph 
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\graph1.txt");  // 25 nodes
		adjArray = file_to_adjacency_array(file);
		// verifying results
		n = Tme1_q1.nbr_of_nodes_and_edges(file)[0];
		for(int key : adjArray.keySet()) {
			System.out.print(key + " : ");
			for(int j : adjArray.get(key)) {
				System.out.print(j+ " ");
			}
			System.out.print("\n");
		}
		System.out.println(); System.out.println();
		
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt");
		adjArray = file_to_adjacency_array(file);  // (~ 1 sec)
		// verifying results
		n = Tme1_q1.nbr_of_nodes_and_edges(file)[0];
		for(int key : adjArray.keySet()) {
			System.out.print(key + " : ");
			for(int j : adjArray.get(key)) {
				System.out.print(j+ " ");
			}
			System.out.print("\n");
		}
		System.out.println(); System.out.println();
		
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-lj.ungraph.txt");
		adjArray = file_to_adjacency_array(file);  // ( > 5 min )
		// verifying result
		for(int node : adjArray.keySet()) {
			System.out.print(node + " : ");
			for(int neighbour : adjArray.get(node)) {
				System.out.print(neighbour + ", ");
			}
			System.out.print("\n");
		}
		System.out.println(); System.out.println();
		
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-orkut.ungraph.txt");
		adjArray = file_to_adjacency_array(file);  // ( > 1h )
		System.out.println("ok"); System.out.println();
		 
		////////// testing adjacency matrix ///////////
		
		file = new File("D:\\NeosD\\Universit\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt"); 
		adjMatrix = file_to_adjacency_matrix(file);  //  OutOfMemoryError ( >8GB )
		// verifying results
		n = Tme1_q1.nbr_of_nodes_and_edges(file)[0];
		for(int i = 0 ; i<n ; i++) {
			System.out.print(i + " : ");
			for(int j = 0 ; j<n ; j++) {
				System.out.print(adjMatrix[i][j] + " ");
			}
			System.out.print("\n");
		}*/
	}
}

