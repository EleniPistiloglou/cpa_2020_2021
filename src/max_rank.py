import random
import time
from zipfile import ZipFile

import numpy as np


path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME3\\"
file_prefix = "cpa_tme3_benchmark_160000"



def max_pagerank():
    """
    prints the names of the 5 best ranked pages
    :return:
    """
    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\"
    a = np.load(path + "tme2_cpa_pagerank_results_a_0.15_iter_40_new.npy")
    max_ranks = []
    indices = []
    initial_size = len(a)
    print("initial size : ", initial_size)

    # we find the page with maximum ranking and then we delete this page from the array
    max_ = max(a)
    max_ranks.append(max_)
    indices.append(np.where(a==max_)[0])
    print(max_)
    pos = np.where(a==max_)
    print(pos)
    l = len(pos[0])
    print(l)
    a = np.delete(a, np.where(a==max_))
    print("new length : ", len(a))

    # we repete until we find the 5 first pages
    while len(a)>initial_size-5:
        max_ = max(a)
        max_ranks.append(max_)
        indices.append(np.where(a == max_)[0])
        a = np.delete(a, np.where(a == max_))
        print("new length : ", len(a))

    print()
    print(max_ranks)
    print(indices)

    # decoding new node names to ids
    a = np.load(path + "new_names.npy")
    ids = []
    for name in indices :
        ids.append(a[name])

    print("ids : ", ids)


#max_pagerank()
