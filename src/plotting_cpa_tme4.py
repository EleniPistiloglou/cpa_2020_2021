"""
plots the graph for tme4 ex2
"""


import matplotlib.pyplot as plt


def affichage(graph_file_prefix = "net"):

    c = 0
    with open("result_ex2_" + graph_file_prefix + ".txt", 'r') as text :
        line = text.readline()
        while line :
            l = line.split(" ")
            plt.plot(int(l[0]), int(l[1]), 'bo')
            c += 1
            if c >= 71857: # the graph is being printed partially to avoid memory errors
                plt.axis('equal')
                plt.show()
                c = 0
            line = text.readline()
        plt.yscale("log")
        plt.xscale("log")
        plt.axis('equal')
        plt.show()


affichage()