"""
implements pagerank on wikipedia graph with a given a and number of iterations
"""

######################## elapsed time ###########################
#
# for iter = 10 :
# 1704.8395748138428  seconds
#
#################################################################


import math
import time
from zipfile import ZipFile
import numpy as np


def node_renaming():
    """
    renames the nodes to create a dense array
    saves the array containing the node id for each new name
    and another array containing the new name for each id
    """

    new_name_to_id = []
    nodes = set()
    with ZipFile("C:\\Users\\elenp\\Downloads\\alr21--dirLinks--enwiki-20071018.zip", "r") as f:
        with f.open("alr21--dirLinks--enwiki-20071018.txt") as text1:
            l = text1.readline()
            l_ = l.decode('ascii')
            while (l_[0] == "#" or l_[0] == "\n"):
                l = text1.readline()
                l_ = l.decode('ascii')
            print("reading file...")
            while (l):
                if (l_[0] != "\n" and l_[0] != "#"):
                    l_split = l_.split("\t")
                    nodes.add(int(l_split[0]))
                    nodes.add(int(l_split[1]))
                l = text1.readline()
                l_ = l.decode('ascii')

    print("length of nodes : ", len(nodes))
    while nodes :
        new_name_to_id.append(nodes.pop())
    print("length of new_names : ", len(new_name_to_id))
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\new_names.npy", np.array(new_name_to_id))

    # constructing array containing the new name of each id
    max_node = 13834639  # max node id
    id_to_new_name = [ -1 for x in range(max_node+1) ]
    for i in range(len(new_name_to_id)) :
        id_to_new_name[new_name_to_id[i]] = i
    print(len(id_to_new_name))
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\ids_to_new_names.npy", np.array(id_to_new_name))



def degrees():
    """
    Stores in an npy file the outdegree of the nodes. The same for the indegree
    """
    nbr_nodes = 2070486
    outdegree = np.zeros(nbr_nodes)
    indegree = np.zeros(nbr_nodes)
    id_to_new_name = np.load("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\ids_to_new_names.npy")

    with ZipFile("C:\\Users\\elenp\\Downloads\\alr21--dirLinks--enwiki-20071018.zip", "r") as f:
        with f.open("alr21--dirLinks--enwiki-20071018.txt") as text1:
            l = text1.readline()
            l_ = l.decode('ascii')
            while (l_[0] == "#" or l_[0] == "\n"):
                l = text1.readline()
                l_ = l.decode('ascii')
            print("reading file...")
            while (l):
                if (l_[0] != "\n" and l_[0] != "#"):
                    l_split = l_.split("\t")
                    a = id_to_new_name[int(l_split[0])]
                    b = id_to_new_name[int(l_split[1])]
                    outdegree[a] += 1
                    indegree[b] += 1
                l = text1.readline()
                l_ = l.decode('ascii')
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\indegree.npy", indegree)
    np.save("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\outdegree.npy", outdegree)



def pagerank(a, iter):

    path = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\"
    nbr_nodes = 2070486 # known from the printing inside node_renaming()
    outdegree = np.load(path + "outdegree.npy")
    id_to_new_name = np.load("D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\ids_to_new_names.npy")

    # parameters
    initial_val = 1/nbr_nodes   # parameter l
    a_ = 1-a
    a_l = initial_val * a  # a*l

    P_init = np.array([initial_val for x in range(nbr_nodes)])
    P_res = np.array([0.0 for x in range(nbr_nodes)])
    dead_ends = np.where(outdegree==0.0)[0]

    starting_time = time.time()

    for t in range(iter):
        with ZipFile("C:\\Users\\elenp\\Downloads\\alr21--dirLinks--enwiki-20071018.zip", "r") as f:
            with f.open("alr21--dirLinks--enwiki-20071018.txt") as text1:
                l = text1.readline()
                l_ = l.decode('ascii')
                while (l_[0] == "#" or l_[0] == "\n"):
                    l = text1.readline()
                    l_ = l.decode('ascii')
                while (l):
                    if (l_[0] != "\n" and l_[0] != "#"):
                        l_split = l_.split("\t")
                        u = id_to_new_name[int(l_split[0])]
                        v = id_to_new_name[int(l_split[1][:-1])]
                        P_res[v] += 1 / outdegree[u] * P_init[u] if outdegree[u] != 0 else 0  # useless condition since there exists an edge uv
                    l = text1.readline()
                    l_ = l.decode('ascii')

        # adding probabilities of landing to i from the dead-end nodes
        sum = 0
        for node in dead_ends:
            sum += 1 / nbr_nodes * P_init[node]
        P_res = np.add(P_res, sum)

        P_res = np.multiply(P_res, a_)
        P_res = np.add(P_res, a_l)

        # normalization and swapping P_res with P_init
        print("iteration ", t, " finished. Normalizing...")
        norm = P_res.sum()
        print("sum : ", norm)
        P_res = np.divide(P_res, norm)
        print("sum after normalization : ", P_res.sum())
        # this normalization seems to have no big effect . . .
        ##################################################
        # iteration  22  finished. Normalizing...
        # sum :  1.0000000000000002
        # sum after normalization :  0.9999999999999994
        #
        # iteration  23  finished. Normalizing...
        # sum :  0.999999999999998
        # sum after normalization :  1.0000000000000004
        #
        ###################################################
        P_init = np.copy(P_res)
        P_res = np.zeros(nbr_nodes)
        print()

    print("processed finished after ", time.time()-starting_time, " seconds")

    np.save(path + "tme2_cpa_pagerank_results_a_"+str(a)+"_iter_"+str(iter)+"_new.npy", P_init)
    print("results have been saved")



node_renaming()
degrees()
pagerank(0.15, 10)
pagerank(0.5, 10)
pagerank(0.9, 10)
pagerank(0.15, 20)
pagerank(0.15, 40)
pagerank(0.15, 80)

