"""
this code uses the results from the program tme4_cpa_ex1.py
to calculate the core value of the graph
as well as the average degree density and the edge density
"""


import numpy as np


def core_value(graph_file):
    core_values = set()
    degrees = []
    with open("result_ex2_" + graph_file + ".txt", 'r') as text_file :
        line = text_file.readline()
        while line :
            s = line.split(" ")
            core_values.add(int(s[1]))
            degrees.append(int(s[0]))
            line = text_file.readline()

    print("the graph's core value is ", max(core_values))

    print("the average degree is ", np.average(np.array(degrees)))


def anomalous_authors():
    """
    detects the anomalous authors aon the google scholar graph
    """

    deg_c_val = []

    with open("result_ex2_net.txt", 'r') as text_file:
        line = text_file.readline()
        while line:
            s = line.split(" ")
            deg_c_val.append([int(s[0]), int(s[1])])
            line = text_file.readline()

    set1 = set() # contains authors with high degree and small coreness
    set2 = set() # contains authors with high coreness and small degree

    for i in range(len(deg_c_val)):
        if ( deg_c_val[i][0] > 85 and deg_c_val[i][1] in [8,6] ) or ( deg_c_val[i][0] > 60 and deg_c_val[i][1] in [9,10,11] ):
            set1.add(i)
        elif deg_c_val[i][1] >= 11:
            set2.add(i)

    print("indices of set1 : ")
    for i in set1:
        print(i)

    print("indices of set2 : ")
    for i in set2:
        print(i)


anomalous_authors()


############### results #################
"""
indices of set1 : 
34756
17639
3596
57681
28177
indices of set2 : 
29187
149891
149892
73228
66448
66450
66454
66456
66457
149879
25912
25913
25914
25915
25916
64190
38472
69707
69708
164820
122714
115559
149866
149867
149868
149869
149871
149872
60017
149873
149874
149876
149877
128119
149880
149881
149882
149883
149884
"""
#########################################