"""
plots the graphs for ex2 of tme2
"""


import numpy as np
import matplotlib.pyplot as plt


def pagerank_plot():

    file_15 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.15_iter_10_new.npy"
    results_15 = np.load(file_15)
    results_15 = results_15[:150000]

    ### graph1 ###

    file_indegree = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\indegree.npy"
    indegree = np.load(file_indegree)
    indegree = indegree[:150000]
    plt.plot(indegree, results_15, 'bo')
    plt.show()

    ### graph2 ###

    file_outdegree = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\outdegree.npy"
    outdegree = np.load(file_outdegree)
    outdegree =outdegree[:150000]
    plt.plot(outdegree, results_15, 'go')
    plt.show()
    
    ### graph3 ###

    file_10 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.1_iter_10_new.npy"
    results_10 = np.load(file_10)
    results_10 = results_10[:150000]

    plt.plot(np.arange(2), np.arange(2), color='grey')
    plt.scatter(results_15, results_10, color = 'green')
    plt.axis('equal')
    plt.show()

    
    ### graph4 ###

    file_50 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.5_iter_10_new.npy"
    results_50 = np.load(file_50)
    results_50 = results_50[:150000]

    plt.plot(results_15, results_50, 'bo')
    plt.plot(np.arange(2), np.arange(2), color = 'grey')
    plt.axis('equal')
    plt.show()

    ### graph5 ###

    file_90 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.9_iter_10_new.npy"
    results_90 = np.load(file_90)
    results_90 = results_90[:150000]

    plt.plot(results_15, results_90, 'o', color = 'orange')
    plt.plot(np.arange(2), np.arange(2), color='grey')
    plt.axis('equal')
    plt.show()

    ### graph6 ###
    
    file_15_iter_20 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.15_iter_20_new.npy"
    results_15_iter_20 = np.load(file_15_iter_20)
    results_15_iter_20 = results_15_iter_20[:150000]
    
    plt.plot(results_15, results_15_iter_20, 'bo')
    plt.plot(np.arange(2), np.arange(2), color='grey')
    plt.axis('equal')
    plt.show()

    file_15_iter_40 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.15_iter_40_new.npy"
    results_15_iter_40 = np.load(file_15_iter_40)
    results_15_iter_40 = results_15_iter_40[:150000]

    plt.plot(results_15, results_15_iter_40, 'bo')
    plt.plot(np.arange(2), np.arange(2), color='grey')
    plt.axis('equal')
    plt.show()

    # plotting the difference
    plt.plot(results_15, np.subtract(results_15_iter_20, results_15), 'o', color='purple')
    plt.plot(results_15, np.zeros(150000), color='grey')
    plt.yscale('log')
    plt.xscale('log')
    plt.show()

    plt.plot(results_15_iter_20, np.subtract(results_15_iter_40, results_15_iter_20), 'o', color='cyan')
    plt.plot(results_15_iter_20, np.zeros(150000), color='grey')
    plt.yscale('log')
    plt.xscale('log')
    plt.show()

    file_15_iter_80 = "D:\\NeosD\\Université\\Master_stl\\CPA\\TME2\\11_3\\tme2_cpa_pagerank_results_a_0.15_iter_40_new.npy"
    results_15_iter_80 = np.load(file_15_iter_80)
    results_15_iter_80 = results_15_iter_80[:150000]

    plt.plot(np.arange(2), np.arange(2), color='grey')
    plt.plot(results_15_iter_40, results_15_iter_80, 'o', color='cyan')
    plt.axis('equal')
    plt.show()



pagerank_plot()