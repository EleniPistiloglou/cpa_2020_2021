package tme1_cpa;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Tme1_q4 {

	public static void triangles(File file) {
		
		HashMap<Integer,ArrayList<Integer>> adj = Tme1_q2.file_to_adjacency_array(file);
		
		// finding max node name
		int max = 0;
		for(int key:adj.keySet())
			if(max<key) max = key;
				
		/* reordering the nodes according to their degree  */
		
		HashMap<Integer,ArrayList<Integer>> degrees = new HashMap<>(); // key:degree values:nodes of degree key	
		
		// filling in a map (degrees) with the lists of nodes having equal degree
		// and finding the maximum degree
		int maxdeg = 0;
		for(int key : adj.keySet()) {
			int deg = adj.get(key).size();
			ArrayList<Integer> nodes;
			if(degrees.containsKey(deg)) 
				nodes = degrees.get(deg); 
			else
				nodes = new ArrayList<>();
			nodes.add(key);
			degrees.put(deg, nodes);
			if(deg>maxdeg) maxdeg=deg;
		}
		
		// putting nodes in an arraylist (order) in the order they will be treated 
		// and storing their order in an array (priority)
		int deg = maxdeg;
		//System.out.println("max degree : "+maxdeg);
		int[] priority = new int[max+1]; // if priority[i] < priority[j] then the node i will be considered earlier from node j
		ArrayList<Integer> order = new ArrayList<>();
		int p = 0 ;
		while(deg>0) {
			ArrayList<Integer> nodes = degrees.get(deg);
			if(nodes != null)
				for(int node : nodes) {
					order.add(node);
					priority[node] = p++;
				}
			deg--;
		}
		
		// sorting the neighbours
		for(int key : adj.keySet()) {
			ArrayList<Integer> neighbours = adj.get(key);
			Collections.sort(neighbours);
			adj.put(key, neighbours);
		}
				
		/* listing triangles in the form of triplets of nodes */
		
		ArrayList<int[]> triangles = new ArrayList<>();
		for(int node : order) {
			p = priority[node];
			//System.out.println("node : "+node+" -> "+p);
			ArrayList<Integer> neighbours = adj.get(node);
			for(int neighbour : neighbours) // for each edge  
			{
				if(priority[neighbour]>p) // avoiding considering an edge twice because the graph is undirected
				{
					//System.out.print("   neighbour : "+neighbour+" -> "+priority[neighbour]+"\n");
					ArrayList<Integer> neighbours2 = adj.get(neighbour);
					int i = 0 ; int j = 0 ;
					int size1 = neighbours.size();
					int size2 = neighbours2.size();
					while(i<size1 && j<size2) {
						if( priority[neighbours.get(i)] <= p ) { i++; continue; }
						if ( priority[neighbours2.get(j)] <= priority[neighbour] ) { j++; continue; }
						//System.out.println("    comparing "+neighbours.get(i)+" , "+neighbours2.get(j));
						if( neighbours.get(i) < neighbours2.get(j) ) i++;
						else if( neighbours.get(i) > neighbours2.get(j) ) j++;
						else {
							triangles.add(new int[]{node, neighbour, neighbours.get(i)});
							//System.out.println("found "+node+" "+i+" "+j);
							i++; j++;
						}
					}
				}
			}
		}//System.out.println("\n");
		
		/*  saving the result in a file  */
		String fileName = file.getName();
		String newName = fileName.substring(0,fileName.length()-4)+"_triangles_.txt";
		FileWriter triangles_file;
		try {
			triangles_file = new FileWriter(newName);
			for(int[] t : triangles) {
				triangles_file.write(t[0]+" "+t[1]+" "+t[2]+"\n");
				//System.out.println(t[0]+" "+t[1]+", "+t[1]+" "+t[2]+", "+t[2]+" "+t[0]);
			}
			triangles_file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(triangles.size()+" triangle(s) detected.");
		
	}
	
	public static void main (String[] args) {
		
		File file;
		/*
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph2.txt");	
		triangles(file);   // ok //
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph1.txt");	
		triangles(file);   // ok //
		
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\graph4.txt");	
		triangles(file);   // ok //
		*/
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-amazon.ungraph.txt");	
		long startTime = System.currentTimeMillis();
		triangles(file);   // ok //
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println(elapsedTime+" milliseconds\n");  // 667129 triangles (3,5 seconds) //
		/*
		file = new File("D:\\NeosD\\Université\\Master_stl\\CPA\\TME1\\com-lj.ungraph.txt");	
		triangles(file);   // OutOfMemoryError //
		*/
	}

}
